import requests
from bs4 import BeautifulSoup
import json
import time
import random
import threading


# https://www.ilmateenistus.ee/ilm/ilmavaatlused/vaatlusandmed/pilved/?lang=en&filter%5BmaxDate%5D=14.12.2021&filter%5BminDate%5D=18.06.2008&filter%5Bdate%5D=13.12.2021&filter%5Bhour%5D=6

headers = {
    "Access-Type": "*/*",
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36"
}

url_test = "https://www.ilmateenistus.ee/ilm/ilmavaatlused/vaatlusandmed/pilved/?lang=en&filter%5BmaxDate%5D=14.12.2021&filter%5BminDate%5D=18.06.2008&filter%5Bdate%5D=13.12.2021&filter%5Bhour%5D="


# CONSTANTS
start_hour = 0
end_hour = 23
year = 2015

# TO-DO: check days in February in leap year
days_in_month = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30, 10:31, 11:30, 12:31}


import os
cwd = os.getcwd()
DATA_PATH = os.path.join(cwd, "DATA")
if not os.path.exists(DATA_PATH):
    os.makedirs(DATA_PATH)

# print(current_data_path)

# Directories structure (TEST):
# DATA
#     YEAR
#         [MONTHS]
#             all data for one month
#         JSON for the whole YEAR

# SCRAPING TO LOCAL HTMLs
def data_scrapper(month):
    end_date = days_in_month[month]
    for day in range(1, end_date+1):
        for hour in range(start_hour, end_hour+1):
            url = f"https://www.ilmateenistus.ee/ilm/ilmavaatlused/vaatlusandmed/pilved/?lang=en&filter%5BmaxDate%5D={day}.{month}.{year}&filter%5BminDate%5D=18.06.2008&filter%5Bdate%5D=13.12.2021&filter%5Bhour%5D={hour}"
            response = requests.get(url, headers=headers)
            src = response.text
            # DIRECTORIES
            #
            # year
            YEAR_PATH = os.path.join(DATA_PATH, str(year))
            if not os.path.exists(YEAR_PATH):
                os.makedirs(YEAR_PATH)
            # month
            MONTH_PATH = os.path.join(YEAR_PATH, str(month))
            if not os.path.exists(MONTH_PATH):
                os.makedirs(MONTH_PATH)

            with open(f"{MONTH_PATH}/cloudy_{day}_{month}_{year}_{hour}.html", "w", encoding="utf-8") as file:
                file.write(src)
            print(f"DATE: {day}/{month}/{year} at H: {hour} saved!")
        print(f"ALL DAY {day}/{month} saved!")
        # sleeptime = random.uniform(1, 3)
        # print(f"Sleeping for {sleeptime}s")
        # time.sleep(sleeptime)
    print(f"MONTH {month} SAVED!")


def json_parser(months):
    print("Beginning scrapping...")
    station_info_per_hour = dict()
    all_stations_info_per_day = list()
    for m in range(months):
        end_date = days_in_month[m]
        for d in range(1, end_date+1):
            for h in range(start_hour, end_hour+1):
                with open(f"{DATA_PATH}/{year}/{m}/cloudy_{d}_12_2021_{h}.html", "r", encoding="utf-8") as file:
                    src = file.read()

                soup = BeautifulSoup(src, "lxml")

                table = soup.find("table")
                html_table_data = table.tbody.find_all("tr")
                # print(html_table_data)

                table_headers = ['Station', 'height_1_layer', 'amount_1_layer',
                                            'height_2_layer', 'amount_2_layer',
                                            'height_3_layer', 'amount_3_layer',
                                            'height_4_layer', 'amount_4_layer',
                                            'vertical_visibility', 'present_weather', 'visibility']

                for tr in html_table_data:
                    all_tds = tr.find_all("td")
                    station_info_per_hour['hour'] = h
                    station_info_per_hour['date'] = f"{d}/{m}/{year}"
                    for i in range(len(table_headers)):
                        td_data = all_tds[i].text.replace('\n', '').strip()
                        station_info_per_hour[table_headers[i]] = td_data
                        # print(f"{table_headers[i]}: {td_data} ({type(td_data)})")

                    all_stations_info_per_day.append(station_info_per_hour)
                    station_info_per_hour = dict()

                print(f"Document: cloudy_{d}_{m}_2021_{h}.html parsed!")

            print(f"Day {d} all parsed")
        print(f"Month {m} all parsed!")
        # Writing to JSON file
    print(f"Writing all YEAR...")
    with open(f"{DATA_PATH}/cloudy_{year}.json", "w", encoding="utf-8") as file:
        json.dump(all_stations_info_per_day, file, indent=4, ensure_ascii=False)

    print(f"JSON saved for year {year} saved!")


def main():
    # Using threads for faster scrapping
    start_month = 1
    end_month = 12
    months = list(range(start_month, end_month+1))
    threads = []
    print(f"SCRAPPING FOR MONTHS: {start_month} - {end_month}, year: {year}")
    for m in months:
        t = threading.Thread(target=data_scrapper, args=(m,))
        t.start()
        threads.append(t)
    # Waiting until threads are finished
    # for t in threads:
    #     t.join()

    print("SCRAPPING DONE")
    # json_parser(months)





if __name__ == '__main__':
    main()


