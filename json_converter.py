import requests
from bs4 import BeautifulSoup
import json
import time
import random
import threading

start_hour = 0
end_hour = 23

# TO-DO: check days in February in leap year
days_in_month = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30, 10:31, 11:30, 12:31}

import os
cwd = os.getcwd()
DATA_PATH = os.path.join(cwd, "DATA_METEO")


def json_parser(year):
    months = list(range(1, 12+1))
    counter_403 = 0
    print(f"Beginning scrapping into JSON for year {year}")
    station_info_per_hour = dict()
    all_stations_info_per_day = list()
    for m in months:
        end_date = days_in_month[m]
        for d in range(1, end_date+1):
            for h in range(start_hour, end_hour+1):
                with open(f"{DATA_PATH}/{year}/{m}/meteodata_{d}_{m}_{year}_{h}.html", "r", encoding="utf-8") as file:
                    src = file.read()

                soup = BeautifulSoup(src, "lxml")

                table = soup.find("table")
                try:
                    html_table_data = table.tbody.find_all("tr")
                except:
                    print(f"403 at file cloudy_{d}_12_2021_{h}, skipping...")
                    counter_403 += 1
                    continue
                # print(html_table_data)

                table_headers = ['Station', 'air_temperature', 'relative_humidity',
                                 'sea_level_pressure', 'pressure_tendendency', 'direction_wind',
                                 'speed_wind', 'gust_wind', 'cloud_amount', 'present_weather_sensor',
                                 'present_weather_observer', 'precipitation', 'visilibity']

                for tr in html_table_data:
                    all_tds = tr.find_all("td")
                    station_info_per_hour['hour'] = h
                    station_info_per_hour['date'] = f"{d}/{m}/{year}"
                    for i in range(len(table_headers)):
                        td_data = all_tds[i].text.replace('\n', '').strip()
                        station_info_per_hour[table_headers[i]] = td_data
                        # print(f"{table_headers[i]}: {td_data} ({type(td_data)})")

                    all_stations_info_per_day.append(station_info_per_hour)
                    station_info_per_hour = dict()

                print(f"Document: meteo_{d}_{m}_{year}_{h}.html parsed!")

            print(f"Day {d} all parsed")
        print(f"Month {m} all parsed!")
        # Writing to JSON file
    print(f"Writing all YEAR...")
    with open(f"{DATA_PATH}/cloudy_{year}.json", "w", encoding="utf-8") as file:
        json.dump(all_stations_info_per_day, file, indent=4, ensure_ascii=False)

    print(f"JSON saved for year {year} saved! Errors {counter_403}")

def main():
    # , 2016, 2017, 2018, 2019, 2020
    years = [2016, 2017, 2018, 2019, 2020]
    threads = []
    for year in years:
        t = threading.Thread(target = json_parser, args = (year,))
        t.start()
        threads.append(t)




    
    

if __name__ == '__main__':
    main()