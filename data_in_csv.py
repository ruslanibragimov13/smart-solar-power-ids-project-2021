import json
import csv
import os

cwd = os.getcwd()
DATA_PATH = os.path.join(cwd, "DATA_METEO")

year = 2020
with open(f"{DATA_PATH}/cloudy_{year}.json", "r", encoding="utf-8") as json_file:
    all_stations_per_day = json.load(json_file)

field_names = list(all_stations_per_day[0].keys())
# print(field_names)
with open(f"{DATA_PATH}/meteodata_{year}.csv", "w", newline='', encoding="utf-8") as csv_file:
    fieldnames = field_names
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

    writer.writeheader()
    for station in all_stations_per_day:
        writer.writerow(station)

