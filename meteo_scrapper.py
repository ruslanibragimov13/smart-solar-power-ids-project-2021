import requests
from bs4 import BeautifulSoup
import json
import time
import random
import threading

# https://www.ilmateenistus.ee/ilm/ilmavaatlused/vaatlusandmed/tunniandmed/?lang=en&filter%5BmaxDate%5D=14.12.2021&filter%5BminDate%5D=30.01.2004&filter%5Bdate%5D=14.12.2021&filter%5Bhour%5D=9

headers = {
    "Access-Type": "*/*",
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36"
}

url_test = "https://www.ilmateenistus.ee/ilm/ilmavaatlused/vaatlusandmed/tunniandmed/?lang=en&filter%5BmaxDate%5D=14.12.2021&filter%5BminDate%5D=30.01.2004&filter%5Bdate%5D=14.12.2021&filter%5Bhour%5D=9"

# CONSTANTS
start_hour = 0
end_hour = 23

# TO-DO: check days in February in leap year
days_in_month = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30, 10:31, 11:30, 12:31}


import os
cwd = os.getcwd()
DATA_PATH = os.path.join(cwd, "DATA_METEO")
if not os.path.exists(DATA_PATH):
    os.makedirs(DATA_PATH)

# Directories structure (TEST):
# DATA
#     YEAR
#         [MONTHS]
#             all data for one month
#         JSON for the whole YEAR


# SCRAPING TO LOCAL HTMLs
def data_scrapper(month, year):
    end_date = days_in_month[month]
    for day in range(1, end_date+1):
        for hour in range(start_hour, end_hour+1):
            url = f"https://www.ilmateenistus.ee/ilm/ilmavaatlused/vaatlusandmed/tunniandmed/?lang=en&filter%5BmaxDate%5D=14.12.2021&filter%5BminDate%5D=30.01.2004&filter%5Bdate%5D={day}.{month}.{year}&filter%5Bhour%5D={hour}"
            response = requests.get(url, headers=headers)
            src = response.text
            # DIRECTORIES FOR .HTML FILES
            #
            # YEAR
            YEAR_PATH = os.path.join(DATA_PATH, str(year))
            if not os.path.exists(YEAR_PATH):
                os.makedirs(YEAR_PATH)
            # MONTH
            MONTH_PATH = os.path.join(YEAR_PATH, str(month))
            if not os.path.exists(MONTH_PATH):
                os.makedirs(MONTH_PATH)

            with open(f"{MONTH_PATH}/meteodata_{day}_{month}_{year}_{hour}.html", "w", encoding="utf-8") as file:
                file.write(src)
            # print(f"DATE: {day}/{month}/{year} at H: {hour} saved!")
            sleeptime_h = random.uniform(0, 2)
            time.sleep(sleeptime_h)
        sleeptime_d = random.uniform(1, 4)
        time.sleep(sleeptime_d)
        print(f"DAY {day}/{month}/{year} saved!")
    print(f"MONTH {month}/{year} SAVED!")

def main():
    # List of years to make it automatic
    years = [2015, 2016, 2017, 2018, 2019, 2020]

    # Using threads for faster scrapping
    start_month = 1
    end_month = 12
    months = list(range(start_month, end_month+1))
    for year in years[::-1]:
        threads = []
        print(f"SCRAPPING FOR MONTHS: {start_month} - {end_month}, year: {year}")
        for m in months:
            t = threading.Thread(target=data_scrapper, args=(m, year))
            t.start()
            threads.append(t)
        for thread in threads:
            thread.join()
        print(f"WEB Parsing done for year {year}!")
    print(f"WEB Parsing for years {years} done!")

    # proceed_options = ["y", "yes", "n", "no"]
    # while True:
    #     proceed = input("Do you want to proceed to converting HTMLs to JSON?(y/N)>> ")
    #     if proceed.lower() not in proceed_options:
    #         continue
    #     else:
    #         break
    #
    # if proceed.lower() == "y" or proceed.lower() == "yes":
    #     pass
    # elif proceed.lower() == "n" or proceed.lower() == "no":
    #     print("Finished.")


if __name__ == '__main__':
    main()


